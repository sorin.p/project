import turtle

w = turtle.Screen()
w.title('Spiral Helix')
w.bgcolor('pink')

colors = ['red', 'green', 'yellow', 'purple', 'orange', 'black']

t = turtle.Pen()
t.speed(1000)

for x in range(400):
    color = colors[x % len(colors)]
    t.pencolor(color)
    t.width(x / 100 + 1)
    t.forward(x)
    t.left(79)

turtle.done()
