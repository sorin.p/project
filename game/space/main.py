import pygame
import random
import math
from pygame import mixer

# Initialize the pygame
pygame.init()

# create the screen
screen = pygame.display.set_mode((800, 600))

# background
background = pygame.image.load("background.png")

# background
mixer.music.load("background.wav")
mixer.music.play(-1)
# Title and icon
pygame.display.set_caption("Space")
icon = pygame.image.load("ufo.png")
pygame.display.set_icon(icon)

# Player
# player = pygame.image.load("player.png")
# icon = pygame.image.load("ufo.png")
# pygame.display.set_icon(icon)
# playerImg = pygame.image.load("player.png")

# score
score_value = 0
font = pygame.font.Font("freesansbold.ttf", 32)  # style : www.dafont.com

textX = 10
textY = 10


def showScore(x, y):
    score = font.render("Score :" + str(score_value), True, (0, 255, 0), (255, 255, 255))
    screen.blit(score, (x, y))


# game over text
over_font = pygame.font.Font("freesansbold.ttf", 64)


def game_over_text():
    over_text = over_font.render("GAME OVER", True, (0, 255, 0), (255, 255, 255))
    screen.blit(over_text, (200, 250))


# Position player
playerImg = pygame.image.load("player.png")
playerX = 370
playerY = 480
playerX_change = 0


def player(x, y):
    screen.blit(playerImg, (x, y))


# Position enemy
enemyImg = []
enemyX = []
enemyY = []
enemyX_change = []
enemyY_change = []
# num_of_enemies = 6
num_of_enemies = random.randint(5, 10)
# enemyImg = pygame.image.load("enemy.png")
# enemyX = random.randint(0, 735)
# enemyY = random.randint(50, 150)
# # speed enemy
# enemyX_change = 2.5
# enemyY_change = 40

for i in range(num_of_enemies):
    enemyImg.append(pygame.image.load("enemy.png"))
    enemyX.append(random.randint(0, 735))
    enemyY.append(random.randint(50, 150))
    # speed enemy
    enemyX_change.append(2.5)
    enemyY_change.append(40)


def enemy(x, y, i):
    screen.blit(enemyImg[i], (x, y))


# Position bullet
bulletImg = pygame.image.load("bullet.png")
bulletX = 0
bulletY = playerY
# speed bullet
bulletX_change = 0
bulletY_change = 10
bullet_state = "ready"


def fire_bullet(x, y):
    global bullet_state
    bullet_state = "fire"
    screen.blit(bulletImg, (x + 16, y + 10))


# dead enemy
def iscollision(enemyX, enemyY, bulletX, bulletY):
    distance = math.sqrt((math.pow(enemyX - bulletX, 2)) + (math.pow(enemyY - bulletY, 2)))
    if distance < 27:
        return True
    else:
        return False


# Game Loop
running = True
while running:
    # RGB - Red, Green, Blue
    # color black:
    # screen.fill((0, 0, 0))
    # color ciad:
    screen.fill((0, 255, 255))
    # background image
    screen.blit(background, (0, 0))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        # keystroke is pressed
        # test
        # if event.type == pygame.KEYDOWN:
        #     if event.key == pygame.K_LEFT:
        #         print("LEFT")
        #     if event.key == pygame.K_RIGHT:
        #         print("RIGHT")
        # if event.type == pygame.KEYDOWN:
        #     if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
        #         print("keystoke has been released")
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = -5
            if event.key == pygame.K_RIGHT:
                playerX_change = 5
            if event.key == pygame.K_SPACE:
                if bullet_state is "ready":
                    bullet_Sound = mixer.Sound("laser.wav")
                    bullet_Sound.play()
                    bulletX = playerX
                    fire_bullet(bulletX, bulletY)

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                playerX_change = 0

    # move player
    playerX += playerX_change  # margin
    if playerX <= 0:
        playerX = 0
    elif playerX >= 736:
        playerX = 736

    # move enemy
    for i in range(num_of_enemies):
        # game over
        if enemyY[i] > 440:
            for j in range(num_of_enemies):
                enemyY[j] = 2000
            game_over_text()
            break

        enemyX[i] += enemyX_change[i]  # margin
        if enemyX[i] <= 0:
            enemyX_change[i] = 4
            enemyY[i] += enemyY_change[i]
        elif enemyX[i] >= 736:
            enemyX_change[i] = -4
            enemyY[i] += enemyY_change[i]

        # collision
        collision = iscollision(enemyX[i], enemyY[i], bulletX, bulletY)
        if collision:
            explosion_Sound = mixer.Sound("explosion.wav")
            explosion_Sound.play()
            bulletY = 480
            bullet_state = "ready"
            # score += 1
            # print(score)
            score_value += 1
            # extra live enemy, restart position
            enemyX[i] = random.randint(0, 735)
            enemyY[i] = random.randint(50, 150)
        enemy(enemyX[i], enemyY[i], i)

    # bullet move
    if bulletY <= 0:
        bulletY = 480
        bullet_state = "ready"

    if bullet_state is "fire":
        fire_bullet(bulletX, bulletY)
        bulletY -= bulletY_change

    # # collision
    # collision = iscollision(enemyX, enemyY, bulletX, bulletY)
    # if collision:
    #     bulletY = 480
    #     bullet_state = "ready"
    #     score += 1
    #     print(score)
    #     # extra live enemy, restart position
    #     enemyX = random.randint(0, 735)
    #     enemyY = random.randint(50, 150)

    player(playerX, playerY)
    showScore(textX, textY)
    pygame.display.update()
