"""
please install:

pip install pygame

py -m pip install -U pygame –user

py -m pip install pygame


update:



"""

import pygame, sys

pygame.init()  # initializes the Pygame
from pygame.locals import *  # import all modules from Pygame
import random
import math
import time

screen = pygame.display.set_mode((800, 600))

# initializing pygame mixer
pygame.mixer.init()

# changing title of the game window
pygame.display.set_caption('Finding Dory')

# changing the logo
logo = pygame.image.load('findingDoryV.2/gameover.png')
pygame.display.set_icon(logo)

############ MAKING INTRO SCREEEN ###########
IntroFont = pygame.font.Font("freesansbold.ttf", 38)


def introImg(x, y):
    intro = pygame.image.load("findingDoryV.2\intro.png")
    screen.blit(intro, (x, y))


def instructionIMG(x, y):
    instruct = pygame.image.load("findingDoryV.2\instruction.png")
    run = True
    while run:
        screen.blit(instruct, (x, y))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False


def aboutIMG(x, y):
    aboutimg = pygame.image.load("findingDoryV.2\About.png")
    run = True
    while run:
        screen.blit(aboutimg, (x, y))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False


# aiciiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
def play(x, y):
    playtext = IntroFont.render("PLAY", True, (255, 0, 0))
    screen.blit(playtext, (x, y))


def ABOUT(x, y):
    aboutText = IntroFont.render("ABOUT", True, (255, 0, 0))
    screen.blit(aboutText, (x, y))


def Instruction(x, y):
    instructionText = IntroFont.render("INSTRUCTION", True, (255, 0, 0))
    screen.blit(instructionText, (x, y))


def introscreen():
    run = True
    pygame.mixer.music.load('findingDoryV.2/startingMusic.mp3')
    pygame.mixer.music.play()
    while run:
        screen.fill((0, 0, 0))
        introImg(0, 0)
        play(100, 450)
        Instruction(280, 450)
        ABOUT(615, 450)

        ####### getting coordinates of mouse cursor #######
        x, y = pygame.mouse.get_pos()

        ###### storing rectangle coordinates (x, y, length, height) by making variables
        button1 = pygame.Rect(60, 440, 175, 50)
        button2 = pygame.Rect(265, 440, 300, 50)
        button3 = pygame.Rect(600, 440, 165, 50)

        ##### Drawing rectangles with stored coorditates of rectangles.######
        ###### pygame.draw.rect takes these arguments (surface, color, coordinates, border) #####
        pygame.draw.rect(screen, (255, 255, 255), button1, 6)
        pygame.draw.rect(screen, (255, 255, 255), button2, 6)
        pygame.draw.rect(screen, (255, 255, 255), button3, 6)

        #### if our cursor is on button1 which is PLAY button
        if button1.collidepoint(x, y):
            ### changing from inactive to active by changing the color from white to red
            pygame.draw.rect(screen, (155, 0, 0), button1, 6)
            #### if we click on the PLAY button ####
            if click:
                countdown()  ## CALLING COUNTDOWN FUNCTION TO START OUR GAME

        #### if our cursor is on button2 which is INSTRUCTION button
        if button2.collidepoint(x, y):
            ### changing from inactive to active by changing the color from white to red
            pygame.draw.rect(screen, (155, 0, 0), button2, 6)
            #### if we click on the INSTRUCTION button ####
            if click:
                instructionIMG(0, 0)  ### DISPLAYING OUR INSTRUCTION IMAGE BY CALLING IT

        #### if our cursor is on button3 which is ABOUT button
        if button3.collidepoint(x, y):
            ### changing from inactive to active by changing the color from white to red
            pygame.draw.rect(screen, (155, 0, 0), button3, 6)
            #### if we click on the ABOUT button ####
            if click:
                aboutIMG(0, 0)  ### DISPLAYING OUR ABOUT IMAGE BY CALLING IT

        ###### checking for mouse click event
        click = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
        pygame.display.update()


###### Countdown ######
def countdown():
    font2 = pygame.font.Font('freesansbold.ttf', 85)
    countdownBacground = pygame.image.load('findingDoryV.2/bg.png')
    three = font2.render('3', True, (187, 30, 16))
    two = font2.render('2', True, (255, 255, 0))
    one = font2.render('1', True, (51, 165, 50))
    go = font2.render('GO!!!', True, (0, 255, 0))

    ##### displaying blank background #####
    screen.blit(countdownBacground, (0, 0))
    pygame.display.update()

    ###### Displaying  three (3) ######
    screen.blit(three, (350, 250))
    pygame.display.update()
    time.sleep(1)

    ##### displaying blank background #####
    screen.blit(countdownBacground, (0, 0))
    pygame.display.update()
    time.sleep(1)

    ###### Displaying  two (2) ######
    screen.blit(two, (350, 250))
    pygame.display.update()
    time.sleep(1)

    ##### displaying blank background #####
    screen.blit(countdownBacground, (0, 0))
    pygame.display.update()
    time.sleep(1)

    ###### Displaying  one (1) ######
    screen.blit(one, (350, 250))
    pygame.display.update()
    time.sleep(1)

    ##### displaying blank background #####
    screen.blit(countdownBacground, (0, 0))
    pygame.display.update()
    time.sleep(1)

    ###### Displaying  Go!!! ######
    screen.blit(go, (300, 250))
    pygame.display.update()
    time.sleep(1)
    gameloop()  # calling the gamloop so that our game can start after the countdown
    pygame.display.update()


# defining our gameloop function
def gameloop():
    ####### music #######
    pygame.mixer.music.load('findingDoryV.2\BackgroundMusic.mp3')
    pygame.mixer.music.play()
    ###### sound effect for collision ######
    crash_sound = pygame.mixer.Sound('findingDoryV.2\car_crash.wav')

    ####### scoring part ######
    score_value = 0
    font1 = pygame.font.Font("freesansbold.ttf", 25)

    def show_score(x, y):
        score = font1.render("SCORE: " + str(score_value), True, (255, 0, 0))
        screen.blit(score, (x, y))

    # highscore part
    with open("findingDoryV.2\highscore.txt", "r") as f:
        highscore = f.read()

    def show_highscore(x, y):
        Hiscore_text = font1.render('HISCORE :' + str(highscore), True, (255, 0, 0))
        screen.blit(Hiscore_text, (x, y))
        pygame.display.update()

    ###### creating our game over function #######

    def gameover():
        gameoverImg = pygame.image.load("findingDoryV.2\gameover.png")
        run = True
        while run:

            screen.blit(gameoverImg, (0, 0))
            time.sleep(0.5)
            show_score(330, 400)
            time.sleep(0.5)
            show_highscore(330, 450)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        countdown()
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit()

    # setting background image
    bg = pygame.image.load('findingDoryV.2/bg.png')

    # setting our player
    mainfish = pygame.image.load('findingDoryV.2\dory.png')
    mainfishX = 350
    mainfishY = 495
    mainfishX_change = 0
    mainfishY_change = 0

    # other cars
    fish1 = pygame.image.load('findingDoryV.2\dory1.png')
    fish1X = random.randint(178, 490)
    fish1Y = 100
    fish1Ychange = 10

    fish2 = pygame.image.load('findingDoryV.2\dory2.png')
    fish1X = random.randint(178, 490)
    fish2Y = 100
    fish2Ychange = 10

    fish3 = pygame.image.load('findingDoryV.2\dory3.png')
    fish3X = random.randint(178, 490)
    fish3Y = 100
    fish3Ychange = 10

    run = True
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                sys.exit()

                # checking if any key has been pressed
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    mainfishX_change += 5

                if event.key == pygame.K_LEFT:
                    mainfishX_change -= 5

                if event.key == pygame.K_UP:
                    mainfishY_change -= 5

                if event.key == pygame.K_DOWN:
                    mainfishY_change += 5

                # checking if key has been lifted up
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    mainfishX_change = 0

                if event.key == pygame.K_LEFT:
                    mainfishX_change = 0

                if event.key == pygame.K_UP:
                    mainfishY_change = 0

                if event.key == pygame.K_DOWN:
                    mainfishY_change = 0

                    # setting boundary for our main car
        if mainfishX < 178:
            mainfishX = 178
        if mainfishX > 490:
            mainfishX = 490

        if mainfishY < 0:
            mainfishY = 0
        if mainfishY > 495:
            mainfishY = 495

        # CHANGING COLOR WITH RGB VALUE, RGB = RED, GREEN, BLUE
        screen.fill((0, 0, 0))

        # displaying the background image
        screen.blit(bg, (0, 0))

        # displaying our main car
        screen.blit(mainfish, (mainfishX, mainfishY))

        # displaing other cars
        screen.blit(fish1, (fish1X, fish1Y))
        screen.blit(fish2, (fish1X, fish2Y))
        screen.blit(fish3, (fish3X, fish3Y))
        # calling our show_score function
        show_score(570, 280)
        # calling show_hiscore function
        show_highscore(0, 0)

        # updating the values
        mainfishX += mainfishX_change
        mainfishY += mainfishY_change

        # movement of the enemies
        fish1Y += fish1Ychange
        fish2Y += fish2Ychange
        fish3Y += fish3Ychange
        # moving enemies infinitely
        if fish1Y > 670:
            fish1Y = -100
            fish1X = random.randint(178, 490)
            score_value += 1
        if fish2Y > 670:
            fish2Y = -150
            fish1X = random.randint(178, 490)
            score_value += 1
        if fish3Y > 670:
            fish3Y = -200
            fish3X = random.randint(178, 490)
            score_value += 1

        # checking if highscore has been created
        if score_value > int(highscore):
            highscore = score_value

        # DETECTING COLLISIONS BETWEEN THE CARS

        # getting distance between our main car and fish1
        def iscollision(fish1X, fish1Y, mainfishX, mainfishY):
            distance = math.sqrt(math.pow(fish1X - mainfishX, 2) + math.pow(fish1Y - mainfishY, 2))

            # checking if distance is smaller than 50 after then collision will occur

            if distance < 50:
                return True
            else:
                return False

        # getting distance between our main car and fish2
        def iscollision(fish2X, fish2Y, mainfishX, mainfishY):
            distance = math.sqrt(math.pow(fish2X - mainfishX, 2) + math.pow(fish2Y - mainfishY, 2))

            # checking if distance is smaller than 50 after then collision will occur
            if distance < 50:
                return True
            else:
                return False

        # getting distance between our main car and fish3
        def iscollision(fish3X, fish3Y, mainfishX, mainfishY):
            distance = math.sqrt(math.pow(fish3X - mainfishX, 2) + math.pow(fish3Y - mainfishY, 2))

            # checking if distance is smaller then 50 after then collision will occur
            if distance < 50:
                return True
            else:
                return False

        ##### giving collision a variable #####

        # collision between mainfish and fish1
        coll1 = iscollision(fish1X, fish1Y, mainfishX, mainfishY)

        # collision between mainfish and fish2
        coll2 = iscollision(fish1X, fish2Y, mainfishX, mainfishY)

        # collision between mainfish and fish3
        coll3 = iscollision(fish3X, fish3Y, mainfishX, mainfishY)

        # if coll1 occur
        if coll1:
            fish1Ychange = 0
            fish2Ychange = 0
            fish3Ychange = 0
            fish1Y = 0
            fish2Y = 0
            fish3Y = 0
            mainfishX_change = 0
            mainfishY_change = 0
            pygame.mixer.music.stop()
            crash_sound.play()
            ###### calling our game over function #######
            time.sleep(1)
            gameover()

        # if coll2 occur
        if coll2:
            fish1Ychange = 0
            fish2Ychange = 0
            fish3Ychange = 0
            fish1Y = 0
            fish2Y = 0
            fish3Y = 0
            mainfishX_change = 0
            mainfishY_change = 0
            pygame.mixer.music.stop()
            crash_sound.play()
            ###### calling our game over function #######
            time.sleep(1)
            gameover()

        # if coll3 occur
        if coll3:
            fish1Ychange = 0
            fish2Ychange = 0
            fish3Ychange = 0
            fish1Y = 0
            fish2Y = 0
            fish3Y = 0
            mainfishX_change = 0
            mainfishY_change = 0
            pygame.mixer.music.stop()
            crash_sound.play()
            ###### calling our game over function #######
            time.sleep(1)
            gameover()

        if fish1Ychange == 0 and fish2Ychange == 0 and fish3Ychange == 0:
            pass

        # writing to our highscore.txt file
        with open("findingDoryV.2\highscore.txt", "w") as f:
            f.write(str(highscore))

        pygame.display.update()


introscreen()
